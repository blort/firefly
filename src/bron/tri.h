# ifndef __ffly__noz__tri__h
# define __ffly__noz__tri__h
# include "../ffint.h"
# include "prim.h"

ff_u16_t ffly_bron_tri2(struct bron_tri2*, ff_u16_t, ff_u32_t, ff_u32_t);
# endif /*__ffly__noz__tri__h*/
